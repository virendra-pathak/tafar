"""This module contains various utility function used in various information extraction from the report.\n

.. moduleauthor:: Virendra Kumar Pathak  <virendra-kumar.pathak@tum.de>

"""

import re
import regex
from langdetect import detect

def detect_language(output_filename):
    """This function detect the language in which report was written.\n
    Only English reports are considered as a part of this work.

    Args:
       output_filename (str): the path of output file name in text format.

    Returns:
       str: language (e.g en)

    Example:
        >>> print detect_language("<out_dir>/2011-11-24DTEGN.F57926614_1page")
        en
    """
    with open(output_filename) as myfile:
        head = [next(myfile) for x in range(30)]
    myfile.close()
    lang = (detect(''.join(head)))
    return lang

def getYearOfReport(r_sheet, doc_id):
    """Returns the year in which report was published.

    Args:
       r_sheet (file): The excel sheet file pointer where metadata is stored. One excel sheet per company in database.
       For example: In case of Deutsche Telekom, the file pointer is of 'Deutsche Telekom AG Metadaten.xlsx'.
       doc_id (int): document id of the report.

    Returns:
       int: year

    Example:
        >>> print(getYearOfReport(<r_sheet pointer>, 57926614))
        2011
    """
    if not doc_id:
        return
    doc_id_number = regex.search(r'(\d+)', doc_id)
    if doc_id_number:
        doc_id_number = int(doc_id_number.group(0))
    else:
        return
    for row_index in range(1, r_sheet.nrows):
        d_id = r_sheet.cell(row_index, 14).value
        #print(d_id, "  ", doc_id)
        if (d_id == doc_id_number):
            date = r_sheet.cell(row_index, 0).value
            year = re.search('\d{4}', date)
            #TODO: what if year regex does not match?
            year = int(year.group(0))
            return year

def getContributorOfReport(r_sheet, doc_id):
    """Returns the contributor of the report.\n
    In the database, in addition to all analyst report of a company, there is also a metadata excel sheet file.\n
    The column index 7 contains the contributor name.

    Args:
       r_sheet (file): pointer to metadata excel sheet
       doc_id (str): document id of the report (at index 14 of metadata excel sheet)

    Returns:
       str: contributor name

    Example:
        >>> print(getContributorOfReport(r_sheet, "57926614")
        UBS Equities
    """
    if not doc_id:
        return
    doc_id_number = regex.search(r'(\d+)', doc_id)
    if doc_id_number:
        doc_id_number = int(doc_id_number.group(0))
    else:
        return
    for row_index in range(1, r_sheet.nrows):
        d_id = r_sheet.cell(row_index, 14).value
        #print(d_id, "  ", doc_id)
        if (d_id == doc_id_number):
            contributor = r_sheet.cell(row_index, 7).value
            return contributor


def updateExcelSheet(r_sheet, w_sheet, doc_id, target_price, decision, evalDecision, evalmultiple_count, language):
    if not doc_id:
        return
    doc_id_number = regex.search(r'(\d+)', doc_id)
    if doc_id_number:
        doc_id_number = int(doc_id_number.group(0))
    else:
        return
    #doc_id_number = int(regex.search(r'(\d+)', doc_id).group(0))
    for row_index in range(1, r_sheet.nrows):
        d_id = r_sheet.cell(row_index, 14).value
        #print(d_id, "  ", doc_id)
        if (d_id == doc_id_number):
            w_sheet.write(row_index, 15, target_price)
            w_sheet.write(row_index, 16, decision)
            #w_sheet.write(row_index, 17, evalDecision)
            w_sheet.write(row_index, 17, evalmultiple_count)
            #print(language)
            w_sheet.write(row_index, 18, str(language))
            #print("Hi I am here")

def grepDCFinFirstPage(outputfile):
    """Apply brute force method for DCF valauation in the report.\n
    Typically, it analyses the first page of the report where false positive is highly unlikely.\n

    Args:
       outputfile (str): file path of report in text format.

    Returns:
       str: DCF valuation method

    Example:
        >>> print(grepDCFinFirstPage("<out_dir>/2011-11-24DTEGN.F57926614_1page"))
        DCF
    """
    if "DCF" in open(outputfile).read():
        #print("AA")
        return "DCF"
    # for hannover & Deutsche bank
    # grep in whole file
    if "embedded value" in open(outputfile[:-6]).read():
        #print("BB")
        return "DCF"

def grepMULinFirstPageSAP(outputfile):
    """Apply brute force method for multiple valuation in reports belonging to SAP company\n

    Args:
       outputfile (str): file path of report in text format.

    Returns:
       str: multiple valuation method

    Example:
        >>> print(grepMULinFirstPageSAP("<filePath>"))
        multiple
    """
    page = open(outputfile).read()
    mul_flag = ["on a blend of", "on a blend of", "equally-weighted", "on an equal blend of", "50:50 mix between", "50:50% mix of pricing", "average of target", "avg. of target", "blend of"]
    if [i for i in mul_flag if i in page]:
        return "multiple"

def grepDCFinFirstPageNatixis(outputfile, company):
    page = open(outputfile).read()
    if ("DCF" in page) and ("DCF method" not in page):
        #print("AA")
        if company == "InfineonTechnologiesAG":
            if ("average between" in page) or ("average of" in page) or ("average for" in page) or ("average over" in page):
                return "DCF + Multiple"
        return "DCF"
    # for hannover & Deutsche bank
    # grep in whole file
    if "embedded value" in open(outputfile[:-6]).read():
        #print("BB")
        return "DCF"

def grepDDMinWholeDocs(outputfile):
    """Apply brute force method for DDM valauation in the report.\n

    Args:
       outputfile (str): file path of report in text format.

    Returns:
       str: DDM method

    Example:
        >>> print(grepDDMinWholeDocs("<filepath>"))
        DDM
    """
    wholeDocs = open(outputfile).read()
    if "dividend discount model" in wholeDocs:
        return "DDM"
    if "DDM" in wholeDocs:
        return "DDM"
