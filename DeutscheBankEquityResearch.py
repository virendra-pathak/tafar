"""Contributor specific module for finding valuation method.\n
Contributor: Deutsche Bank Equity Research\n

.. moduleauthor:: Virendra Kumar Pathak <virendra-kumar.pathak@tum.de>

"""

import sys
import itertools
import regex
import utility


def DeutscheBankEquityResearch(year, company, output_filename):
    """ Returns the valaution method used in report when contributor is Deutsche Bank.

    Args:
       year (int): year in which report was published
       company (str): the name of the company
       output_filename (str): path of report in text format

    Returns:
       str: valuation method.

    Example:
     >>> print(DeutscheBankEquityResearch(2013, "Daimler", "<>/2013-02-07DAIGN.DE62325591"))
     Multiples DCF

    Note:
       Database contains reports from 2005-16. The whole set is divided into 2 parts:
       (1) reports until year 2013 (2) reports published between 2013-16.

       Matching templates were found during these time periods.
       There are also some companies where template were invariant of the year.
       For such companies DeutscheBank2016() is used in all the cases.

       Then as per the year and/or company name, this function call other internal function
       dedicated to handle report within their specifc time period (e.g. DeutscheBank2016()).
    """
    eval = ''
    if company == "Allianz":
        return "DCF"
    if company == "VolkswagenAG" or company == "BMW" or company == "Daimler" or company == "MAN" or company == "Hannover" or company == "InfineonTechnologiesAG" or company == "SAPAG" or company == "SiemensAG" or company == "AltanaAG" or company == "UniCreditBankAG" or company == "FreseniusSECoKGaA":
        return DeutscheBank2016(year, company, output_filename)
    if year < 2013:
        eval = DeutscheBank2013(year, company, output_filename)
    else:
        eval = DeutscheBank2016(year, company, output_filename)
    return eval


# multiple_keyword = ['PE ratios', 'multiples', 'PE', 'P/E', 'peer group', 'peers', '\d*\.\d+x', '\d+x', 'EV/EBIT']
multiple_keyword = ['Multiple', 'multiplier', '\d*\.\d+x', '\d+x']
# dcf_keyword = ['DCF derived', 'WACC', 'discounted cash flow', 'DCF']
dcf_keyword = ['next three years', 'normalised assumptions thereafter', 'standard base case cost of equity', 'Growth']

# Company: VW
# dcf_keyword_VW = ['ROIC', 'ROIC/IC', 'WACC', 'EV/IC', 'ROIC/WACC', 'DCF-based', 'EV/IC']
dcf_keyword_VW = ['WACC', 'EV/IC', 'ROIC/WACC', 'DCF-based', 'DCF']
multiple_keyword_VW = ['PER', 'P/E', 'PE ', 'EV/Sales', 'multiple', 'times EBIT', 'EV/EBIT']
valuation_header_VW = ['valuation and risks', 'Our price target remains', 'We keep our sell recommendation',
                       'Recommendation: We keep our Sell with', 'Valuation and risks', 'Recommendation remains sell',
                       'Valuation and Risks', 'Upgrading Price Target to Euro', 'Valuation/Risk', 'Valuation/risk',
                       'Valuation & Risk', 'Valuation and risk', 'Valuation:', 'Valuation& risks', 'Valuation / Risk',
                       "Valuation full relative to peers", "Valuation;", "Upgrade to Buy;"]


# BMW
# Valuation_header is similar to VW 
# Share price target of Euro 40;
# TP lowered to EUR25;


# ----------------------------------------
def DeutscheBankERBase(header, output_filename, dcf_key, mul_key, company):
    # print(sys._getframe().f_code.co_name)
    eval = ''
    # print(header)
    with open(output_filename) as f:
        for line in f:
            if [i for i in header if i in line]:
                numOfLineOfIntArea = 50
                if company == "BMW" or company == "Daimler" or company == "MAN" or company == "Hannover" or company == "InfineonTechnologiesAG" or company == "SiemensAG":
                    numOfLineOfIntArea = 5
                if company == "SAPAG":
                    numOfLineOfIntArea = 13
                    if "...." in line:
                        continue

                valuation_para = list(itertools.islice(f, numOfLineOfIntArea))
                if valuation_para:
                    if company == "InfineonTechnologiesAG" or company == "SAPAG" or company == "SiemensAG":
                        valuation_para.insert(0, line)
                    multiple = regex.compile('|'.join('(?:{0})'.format(x) for x in mul_key))
                    is_multiple = multiple.findall(''.join(valuation_para))

                    dcf = regex.compile('|'.join('(?:{0})'.format(x) for x in dcf_key))
                    is_dcf = dcf.findall(''.join(valuation_para))
                    # print("\n".join(valuation_para))
                    # print(is_multiple)
                    if is_multiple:
                        eval = "Multiples"
                    if is_dcf:
                        # print("dcf: ", is_dcf)
                        eval = eval + " " + "DCF"
                    dcf_grep = utility.grepDCFinFirstPage(output_filename + "_1page")
                    if dcf_grep == "DCF":
                        if company == "InfineonTechnologiesAG" or company == "SiemensAG" or company == "FreseniusSECoKGaA":
                            if "DCF" not in eval:
                                eval = "DCF"
                            if company == "FreseniusSECoKGaA":
                                if utility.grepMULinFirstPageSAP(output_filename + "_1page") == "multiple":
                                    if "Multiples" not in eval:
                                        eval = eval + "Multiple"
                        elif company == "SAPAG":
                            multiple = utility.grepMULinFirstPageSAP(output_filename + "_1page")
                            if multiple == "multiple":
                                eval = "Multiples DCF"
                            else:
                                if "DCF" not in eval:
                                    eval = "DCF"
                        else:
                            eval = "DCF"
                        is_dcf = ["DCF"]
                    if is_multiple or is_dcf:
                        return eval.strip()
                else:
                    print("Error:", sys._getframe().f_code.co_name)
                    print("file:", output_filename)
    dcf_grep = utility.grepDCFinFirstPage(output_filename + "_1page")
    if dcf_grep == "DCF":
        if company == "SAPAG" or company == "FreseniusSECoKGaA":
            multiple = utility.grepMULinFirstPageSAP(output_filename + "_1page")
            if multiple == "multiple":
                return "Multiples DCF"
            else:
                return "DCF"
        return "DCF"
    return "None"


# -------------------------------------------
def DeutscheBank2016(year, company, output_filename):
    """ Returns the valuation method for reports by Deutsche Bank published b/w 2013-16.

    Args:
       year (int): year in which report was published
       company (str): the name of the company
       output_filename (str): path of report in text format

    Returns:
       str: valuation method.

    Example:
        >>> print(DeutscheBank2016(2015, "company_name", "<report_file_path>"))
        Multiples

    Warning:
        Please do not call this function directly.\n
        Instead use parent function DeutscheBankEquityResearch() in the parser.\n
    """
    # print(sys._getframe().f_code.co_name)
    valuation_header_keyword2016 = ["Valuation and Risks", "Valuation and risks", "Valuation & Risks",
                                    "We value the group on a sum of the parts basis", "Valuation methodology and Risks",
                                    "Valuation, risks and updated group earnings forecasts"]
    if company == "VolkswagenAG" or company == "BMW" or company == "Daimler" or company == "MAN" or company == "Hannover" or company == "InfineonTechnologiesAG" or company == "SAPAG" or company == "SiemensAG" or company == "AltanaAG" or company == "UniCreditBankAG" or company == "FreseniusSECoKGaA":
        header = valuation_header_VW
        dcf_key = dcf_keyword_VW
        mul_key = multiple_keyword_VW
        if company == "Hannover" or company == "InfineonTechnologiesAG" or company == "SAPAG" or company == "SiemensAG" or company == "AltanaAG" or company == "UniCreditBankAG" or company == "FreseniusSECoKGaA":
            dcf_key = dcf_keyword + dcf_keyword_VW
            if company == "InfineonTechnologiesAG":
                header = header + ["equal blend"]
            if company == "SAPAG":
                header = header + ["Valuation methodology"]
            if company == "SiemensAG":
                header = header + ["Valuation", "our TP based", "TP is based on the", "TP based on"]
            if company == "UniCreditBankAG":
                dcf_key = dcf_keyword_VW

    else:
        header = valuation_header_keyword2016
        dcf_key = dcf_keyword
        mul_key = multiple_keyword
    return DeutscheBankERBase(header, output_filename, dcf_key, mul_key, company)


# -------------------------------------------------
def DeutscheBank2013(year, company, output_filename):
    """ Returns the valaution method for reports by Deutsche Bank published until 2013.

    Args:
       year (int): year in which report was published
       company (str): the name of the company
       output_filename (str): path of report in text format

    Returns:
       str: valuation method.

    Example:
        >>> print(DeutscheBank2013(2012, "company_name", "<report_file_path>"))
        DCF

    Warning:
        Please do not call this function directly.\n
        Instead use parent function DeutscheBankEquityResearch() in the parser.\n
    """
    # print(sys._getframe().f_code.co_name)
    header = ''
    valuation_header_keyword2013 = ["Allianz – sum of the parts valuation",
                                    "Allianz – Summary Sum of the parts valuation",
                                    "Allianz – Sum of the parts valuation", "Allianz –Sum of the parts valuation",
                                    "Allianz – Summary SoTP valuation", "Allianz – Summary valuation"]
    header = valuation_header_keyword2013

    # if the first page contain Q\d preview, than its do not contains the summary table. hard to guess
    if year <= 2008:
        with open(output_filename) as f:
            if regex.findall("Q\d preview", ''.join(list(itertools.islice(f, 40)))):
                header = ["Valuation and risks"]
                eval_result = DeutscheBankERBase(header, output_filename)
                if eval_result == "None":
                    header = valuation_header_keyword2013
                    return DeutscheBankERBase(header, output_filename, company)
                else:
                    return eval_result
    # print("VKP: ", header)
    return DeutscheBankERBase(header, output_filename, company)

# output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp/2006-03-08ALVG.DE36700091"
# output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_BMW/2007-11-02BMWG.DE40823002"
# output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_FreseniusSECoKGaA/2015-10-30FREG.DE72181249"
# print(DeutscheBankEquityResearch(2015, "FreseniusSECoKGaA", output_filename))
