#parser file

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from pdfminer.pdftypes import resolve1
#from cStringIO import StringIO
from io import StringIO

def convert_pdf_to_txt(path):
	rsrcmgr = PDFResourceManager()
	retstr = StringIO()
	codec = 'utf-8'
	laparams = LAParams()
	device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
	fp = open(path, 'rb')
	interpreter = PDFPageInterpreter(rsrcmgr, device)
	password = ""
	#maxpages = 0
	maxpages = 1
	caching = True
	pagenos=set()

	for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
		print("page number", page.pageid)
		page.contents[0].resolve().decode()
		print("page contents length", len(page.contents))
		#print("page data", page.contents[0].resolve().data)
		#print("page contents doc", page.contents[0].doc, "page contents objid", page.contents[0].objid)		
		interpreter.process_page(page)

	text = retstr.getvalue()
	print("Bye")
	fp.close()
	device.close()
	retstr.close()
	#print(text)
	return text

path = "/media/virendra/data/study/2sem/idp/code-repo/tafar/dataset/2005-04-19SIEGn.DE34797908.pdf"
convert_pdf_to_txt(path)
