"""Contributor specific module for finding valuation method.\n
Contributor: UBS

.. moduleauthor:: Virendra Kumar Pathak <virendra-kumar.pathak@tum.de>

"""
import sys
import itertools
import regex
import utility

def UBSEvaluation(year, company, output_filename):
    """ Returns the valaution method used in report when contributor is UBS.

    Args:
       year (int): year in which report was published
       company (str): the name of the company
       output_filename (str): path of report in text format

    Returns:
       str: valuation method

    Example:
        >>> print(UBSEvaluation(2016, "DeutscheTelekomAG", "<path>/2016-08-11DTEGN.F75471809"))
        DCF

    Note:
        Database contains reports from 2005-16.

        The whole set is divided into 2 parts:\n
        1) reports untill year 2014\n
        2) reports published between 2014-16.\n

        The reason for such division was that some matching templates were found during these time periods.
        There are also some companies where template were invariant of the year.
        For such companies UBSEval2016() in all the cases.

        Then as per the year and/or company name, this function call other internal function
        dedicated to handle report within their specific time period (e.g. UBSEval2016()).
    
    """
    eval = ''
    if company == "VolkswagenAG" or company == "BMW" or company == "Daimler" or company == "MAN" or company == "Hannover" or company == "ContinentalAG" or company == "KSAG" or company == "MetroAG" or company == "InfineonTechnologiesAG" or company == "SiemensAG" or company == "AltanaAG" or company == "FreseniusSECoKGaA":
        return UBSEval2016(year, company, output_filename)

    if year < 2014:
        eval = UBSEval2014(year, company, output_filename)
    else:
        eval = UBSEval2016(year, company, output_filename)
    return eval

multiple_keyword = ['PE ratios', 'multiples', 'PE', 'P/E', 'peer group', 'peers', '\d*\.\d+x', '\d+x', 'EV/EBIT']
dcf_keyword = ['DCF derived', 'WACC', 'discounted cash flow', 'DCF']
## VW is same BMW
multiple_keyword_VW = ['P/E', 'EV/t', 'EBITDA multiples', 'EV/EBITDA', 'PE-derived', 'multiples', 'EV/EBIT', 'multiple', 'target PE', 'EV/Sales', 'PE']
dcf_keyword_VW = ['EV/IC', 'DCF', 'DCF model', 'longer-term cash generation', 'DCF-derived', 'DCF based', 'DCF methodology', 'WACC']

valuation_header_keyword2014 = ["Valuation:", "Valuation PT", "Valuation price target", "Our valuation range is based on", "Our PT is based on"]
def UBSEval2014(year, company, output_filename):
    #print(sys._getframe().f_code.co_name)
    eval = ''
    with open(output_filename) as f:
        for line in f:
            if [i for i in valuation_header_keyword2014 if i in line]:
                valuation_para = list(itertools.islice(f, 50))
                if valuation_para:
                    multiple = regex.compile('|'.join('(?:{0})'.format(x) for x in multiple_keyword))
                    is_multiple = multiple.findall(''.join(valuation_para))

                    dcf = regex.compile('|'.join('(?:{0})'.format(x) for x in dcf_keyword))
                    is_dcf = dcf.findall(''.join(valuation_para))
                    #print(''.join(valuation_para))
                    if is_multiple:
                        eval = "Multiples"
                    if is_dcf:
                        eval = eval+" "+"DCF"
                    if is_multiple or is_dcf:
                        return eval.strip()
                else:
                    print("Error:", sys._getframe().f_code.co_name)
                    print("file:", output_filename)
    return "None"

valuation_header_keyword2016 = ["Valuation Method and Risk Statement", "Valuation:", "Summary and valuation"]
valuation_header_VW = ["Valuation PT ", "Our SOTP model gets us to", "Valuation:", "Valuation","DCF based", "Valuation –", "our DCF", "VW – Ord correction likely", "Valuation Method and Risk Statement", "Valuation Method and Risk Statement", "Outlook & Valuation", "Summary and valuation", "Valuation and price target basis"]

#valuation_header_keyword2016 = ["Valuation Method and Risk Statement"]
def UBSEval2016(year, company, output_filename):
    eval = ''
    #print(sys._getframe().f_code.co_name)
    header = valuation_header_keyword2016
    if year == 2016 and company == "Allianz":
        header = ["Valuation Method and Risk Statement"]

    if company == "VolkswagenAG" or company == "BMW" or company == "Daimler" or company == "MAN" or company == "Hannover" or company == "ContinentalAG" or company == "KSAG" or company == "MetroAG" or company == "InfineonTechnologiesAG" or company == "SiemensAG" or company == "AltanaAG" or company == "FreseniusSECoKGaA":
        header = valuation_header_VW
        mul_key = multiple_keyword_VW
        dcf_key = dcf_keyword_VW
        if company == "KSAG" or company == "MetroAG":
            mul_key = mul_key + ["EBITDA"]
        if company == "FreseniusSECoKGaA":
            header = header + ["Fresenius SOP", "Sum of the parts"]
    else:
        mul_key = multiple_keyword
        dcf_key = dcf_keyword

    with open(output_filename) as f:
        for line in f:
            if [i for i in header if i in line]:
                numOfLines = 50
                if company == "VolkswagenAG":
                    numOfLines = 20
                if company == "VolkswagenAG" and year == 2006:
                    numOfLines = 5
                if company == "BMW" or company == "Daimler" or company == "MAN" or company == "Hannover" or company == "ContinentalAG" or company == "KSAG" or company == "MetroAG" or company == "InfineonTechnologiesAG" or company == "SiemensAG" or company == "AltanaAG" or company == "FreseniusSECoKGaA":
                    numOfLines = 10
                valuation_para = list(itertools.islice(f,numOfLines))
                if valuation_para:
                    # added for VW
                    if company == "VolkswagenAG" or company == "BMW" or company == "Daimler" or company == "MAN" or company == "Hannover" or company == "ContinentalAG" or company == "KSAG" or company == "MetroAG" or company == "InfineonTechnologiesAG" or company == "SiemensAG" or company == "AltanaAG" or company == "FreseniusSECoKGaA":
                        valuation_para.insert(0, line)
                    multiple = regex.compile('|'.join('(?:{0})'.format(x) for x in mul_key))
                    is_multiple = multiple.findall(''.join(valuation_para))

                    dcf = regex.compile('|'.join('(?:{0})'.format(x) for x in dcf_key))
                    is_dcf = dcf.findall(''.join(valuation_para))
                    #print("\n".join(valuation_para))
                    #print(is_multiple)
                    if is_multiple:
                        eval = "Multiples"
                    if is_dcf:
                        eval = eval+" "+"DCF"
                        if company == "BMW" or company == "Daimler":
                            onlyDCF = ["DCF-based", "DCF based", "DCF basis"]
                            if [x for x in onlyDCF if x in ''.join(valuation_para)]:
                                eval = "DCF"
                    dcf_grep = utility.grepDCFinFirstPage(output_filename+"_1page")
                    if dcf_grep == "DCF":
                        is_dcf = ["DCF"]
                        # Actually company is SalzgitterAG, but profile is same for the time being
                        #if company == "Hannover" or company == "KSAG":
                        if company == "Hannover" or company == "MetroAG" or company == "SiemensAG" or company == "AltanaAG":
                            if "DCF" not in eval:
                                eval = "DCF"
                        else:
                            eval = "DCF"
                    if is_multiple or is_dcf:
                        return eval.strip()
                else:
                    print("Error:", sys._getframe().f_code.co_name)
                    print("file:", output_filename)
    return "None"

#output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp/2016-06-30ALVG.DE74991869"
#output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_VolkswagenAG/2012-02-28VOWG.DE58912210"
#output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_Hannover/2007-11-09HNRGn.DE40887079"
#print(UBSEvaluation(2016, "MAN", output_filename))
