import os,re
import subprocess
from xlutils.copy import copy
import xlwt,xlrd
import regex

database_dir = "/media/virendra/data/study/2sem/idp/code-repo/tafar/dataset/DeutscheBankAG"
#database_dir = "/media/virendra/data/study/2sem/idp/code-repo/tafar/dataset/SiemensAG"
#database_dir = "/media/virendra/data/study/2sem/idp/code-repo/tafar/dataset/DeutscheBankAG"
output_dir = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_DeutscheBankAG/"
#output_dir = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp2/"

TP_filter_1 = ["Price Target", "Price target", "Target", "price target"]
TP_filter_2 = ["Price"]
TP_filter_3 = [" TP "]

#TP_regex_1 = "[^$](\d*\.\d+|\d+)"
TP_regex_1 = "[^$](\d*\.\d+)"
TP_regex_2 = "\p{Sc}?(\d*\.\d+|\d+)$"
TP_regex_3 = "\s?TP\s\p{Sc}?(\d*\.\d+|\d+)"
TP_regex_4 = "\p{Sc}?(\d*\.\d+|\d+)/"

def extract_Tprice_from_line_firstTime(line, reg_ex):
	target_price = ''
	if "$" not in line:
		target_price = regex.findall(reg_ex, line)
		if target_price:
			target_price = ''.join(target_price[0])
	else:
		target_price = regex.findall(TP_regex_4, line)
		if target_price:
			target_price = ''.join(target_price[0])
	return target_price

def extract_Tprice_from_line(line, reg_ex):
	target_price = ''
	if "$" not in line:
		target_price = regex.findall(reg_ex, line)
		if target_price:
			target_price = ''.join(target_price[-1])
	else:
		target_price = regex.findall(TP_regex_4, line)
		if target_price:
			target_price = ''.join(target_price[-1])
	return target_price

def is_line_contains_TP_trigger_word(line, filter_list):
	if any(word in line for word in filter_list):
		return True
	else:
		return False
	
# Price Target, Price target, TP
def extract_target_price(output_filename):
	target_price = ''
	with open(output_filename) as f:
		for line in f:
			#------------------------------
			# "Price Target" or "Price target"
			if is_line_contains_TP_trigger_word(line, TP_filter_1):
				line = regex.split(regex.compile("|".join(TP_filter_1)), line, 1)[1]
				target_price = extract_Tprice_from_line_firstTime(line.strip(), TP_regex_1)
				if not target_price:
					line = next(f)
					target_price = extract_Tprice_from_line(line.strip(), TP_regex_2)
					if not target_price:
						line = next(f)
						target_price = extract_Tprice_from_line(line.strip(), TP_regex_2)
				if target_price:
					break
				# TODO: move 2 line back as a else part

			# " TP "
			if is_line_contains_TP_trigger_word(line, TP_filter_3):
				#target_price = extract_Tprice_from_line(line.strip(), TP_regex_3)
				line = regex.split(regex.compile("|".join(TP_filter_3)), line, 1)[1]
				target_price = extract_Tprice_from_line_firstTime(line, TP_regex_3)
				#print(line, "target price", target_price)
				if target_price:
					break
			
			# "Price". Beware sometime Price refers to old price not Target Price
			if is_line_contains_TP_trigger_word(line, TP_filter_2):
				line = regex.split(regex.compile("|".join(TP_filter_2)), line, 1)[1]
				price = extract_Tprice_from_line_firstTime(line.strip(), TP_regex_1)
				#file_current_pos = f.tell()
				#----------------------
				for Lineiter in range(1,3):
					line = next(f)
					if is_line_contains_TP_trigger_word(line, TP_filter_1):
						line = regex.split(regex.compile("|".join(TP_filter_1)), line, 1)[1]
						target_price = extract_Tprice_from_line_firstTime(line.strip(), TP_regex_1)
						if not target_price:
							line = next(f)
							target_price = extract_Tprice_from_line(line.strip(), TP_regex_2)
							if not target_price:
								line = next(f)
								target_price = extract_Tprice_from_line(line.strip(), TP_regex_2)
						if target_price:
							break
				#-------------------------
				if target_price:
					break
				else:
					if price:
						target_price = price
						break
			#-------------------------------
	f.close()
	return target_price

shareDecision = ["Buy", "Hold", "Neutral", "Overweight"]
def extract_buysell_decision(output_filename):
	decision = ''
	with open(output_filename) as f:
		for line in f:
			if "Buy" in line:
				decision="Buy"
				break
			if "Overweight" in line:
				decision="Overweight"
				break
			if "Hold" in line:
				decision="Hold"
				break
			if "Neutral" in line:
				decision="Neutral"
				break
			if "Underweight" in line:
				decision="Underweight"
				break
			if "Add" in line:
				decision="Add"
				break
			if "Reduce" in line:
				decision="Reduce"
				break
	f.close()
	return decision

#def is_line_contains_TP_trigger_word(line, filter_list):
#	if any(word in line for word in filter_list):
#		return True
#	else:
#		return False
evalDecision = ["DCF based", "DCF-based", "our DCF", "a DCF", "WACC"]
def extract_evaluation_method(output_filename):
	eval_method = ''
	with open(output_filename) as f:
		for line in f:
			if "DCF based" in line:
				eval_method = "DCF"
				break
			if "DCF-based" in line:
				eval_method = "DCF"
				break
			if "DCF derived" in line:
				eval_method = "DCF"
				break
			if "DCF-derived" in line:
				eval_method = "DCF"
				break
			if "our DCF" in line:
				eval_method = "DCF"
				break
			if "a DCF" in line:
				eval_method = "DCF"
				break
			if "the DCF" in line:
				eval_method = "DCF"
				break
			if "and DCF" in line:
				eval_method = "DCF"
				break
			if "of DCF" in line:
				eval_method = "DCF"
				break
			if "DCF," in line:
				eval_method = "DCF"
				break
			if "WACC" in line:
				eval_method = "DCF"
				break
			if "DCF basis" in line:
				eval_method = "DCF"
				break
			if "DCF analysis" in line:
				eval_method = "DCF"
				break
			if "DCF model" in line:
				eval_method = "DCF"
				break
			if "DCF scenarios" in line:
				eval_method = "DCF"
				break
			if "DCF valuation" in line:
				eval_method = "DCF"
				break
			if regex.findall(r'DCF(?!\smethod)', line):
				eval_method = "DCF"
				break
	return eval_method
			
def updateExcelSheet(doc_id, target_price, decision, evalDecision):
	if not doc_id:
		return
	doc_id_number = regex.search(r'(\d+)', doc_id)
	if doc_id_number:
		doc_id_number = int(doc_id_number.group(0))
	else:
		return
	#doc_id_number = int(regex.search(r'(\d+)', doc_id).group(0))
	for row_index in range(1, r_sheet.nrows):
		d_id = r_sheet.cell(row_index, 14).value
		#print(d_id, "  ", doc_id)
		if (d_id == doc_id_number):
			w_sheet.write(row_index, 15, target_price)
			w_sheet.write(row_index, 16, decision)
			w_sheet.write(row_index, 17, evalDecision)
			#print("Hi I am here")

#main function
file_name = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_DeutscheBankAG/Deutsche Bank AG Metadaten.xlsx"
#file_name = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp2/Deutsche Bank AG Metadaten.xlsx"
rb = xlrd.open_workbook(file_name)
r_sheet = rb.sheet_by_index(0)
wb = copy(rb)
w_sheet = wb.get_sheet(0)
#Document ID, row number 5
w_sheet.write(4, 15, 'Target Price')
w_sheet.write(4, 16, 'Decision')
w_sheet.write(4, 17, 'Evaluation Method')
print("why are you not writing?")


for file in os.listdir(database_dir):
	if file.endswith(".pdf"):
		#extracting document id e.g.DE48994574
		document_id = re.findall(r'\.(.*?)\.',file)
		if document_id:
			document_id = document_id[0]
		#extracting the pdf first page
		full_file_path = os.path.join(database_dir, file)
		#--------------------------
		file_basename = os.path.splitext(file)[0]
		output_filename = output_dir+file_basename
		#--------------------
		#cmd = ["pdftotext", "-l", "1", "-layout", full_file_path,output_filename]
		cmd = ["pdftotext", "-layout", full_file_path,output_filename]
		#print(full_file_path)
		#print(output_filename)
		text = subprocess.check_output(cmd, universal_newlines=True,stderr=subprocess.STDOUT)
		#print(text)
		#extracting the target price
		target_price = extract_target_price(output_filename)
		shareDecision = extract_buysell_decision(output_filename)
		evalDecision = extract_evaluation_method(output_filename)
		#print(shareDecision)
		updateExcelSheet(document_id, target_price, shareDecision, evalDecision)

#book.save("report.xls")
wb.save(file_name + '.out' + os.path.splitext(file_name)[-1])
