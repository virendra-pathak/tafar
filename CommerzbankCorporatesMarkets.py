"""Contributor specific module for finding valuation method.\n
Contributor: Commerzbank Corporates & Markets

.. moduleauthor:: Virendra Kumar Pathak <virendra-kumar.pathak@tum.de>

"""
import sys
import itertools
import regex
import utility

def CommerzbankCorporatesMarkets(year, company, output_filename):
    """ Returns the valaution method used in report when contributor is Commerzbank.

    Args:
       year (int): year in which report was published
       company (str): the name of the company
       output_filename (str): path of report in text format

    Returns:
       str: valuation method.

    Example:
     >>> print(CommerzbankCorporatesMarkets(2014, "DeutscheTelekomAG", "<>/2014-11-06DTEGN.DE68335250"))
     DCF

    Note:
        Database contains reports from 2005-16. The whole set is divided into 2 parts:
        1) reports until year 2013 2) reports published between 2013-16.

        The reason for such division was that some matching templates were found during these time periods.
        There are also some companies where template were invariant of the year.
        For such companies Commerzbank2016() is used in all the cases.

        Then as per the year and/or company name, this function call other internal function dedicated to
        handle report within their specific time period (e.g. Commerzbank2016()).
    
    """
    eval = ''
    if year < 2013:
        eval = Commerzbank2013(year, company, output_filename)
    else:
        eval = Commerzbank2016(year, company, output_filename)
    return eval

#multiple_keyword = ['PE ratios', 'multiples', 'PE', 'P/E', 'peer group', 'peers', '\d*\.\d+x', '\d+x', 'EV/EBIT']
multiple_keyword = ['P/E', 'multiple', 'Multiple', 'multiplier', '\d*\.\d+x', '\d+x', 'peer group-derived target price']
multiple_volkswagen = ["peer valuations", "EV/EBIT", "EV/EBITDA", "EV/sales", "multiple", "PE-ratio", "P/E ratio", "P/E-ratio", "EV/EBIT", "EV/Sales", "valuation multiples", "Peer multiples", "peer valuation",  "peer multiples", "EV/sales", "peer group"]
#dcf_keyword = ['DCF derived', 'WACC', 'discounted cash flow', 'DCF']
dcf_keyword = ['DCF derived', 'WACC', 'discounted cash flow', 'DCF', 'sum-of-the-parts-model','sum of the parts', 'sum-of-the-parts','sum-of-the', 'DDM', 'dividend discount model']

#----------------------------------------
def CommerzbankERBase(header, output_filename, dcf_key, multiple_key, company):
    #print(sys._getframe().f_code.co_name)
    eval = ''
    with open(output_filename) as f:
        for line in f:
            if [i for i in header if i in line]:
                num_of_line = 20
                if company == "EONSE":
                    num_of_line = 50
                valuation_para = list(itertools.islice(f, num_of_line))
                valuation_para.insert(0, line)
                if valuation_para:
                    multiple = regex.compile('|'.join('(?:{0})'.format(x) for x in multiple_key))
                    is_multiple = multiple.findall(''.join(valuation_para))

                    dcf = regex.compile('|'.join('(?:{0})'.format(x) for x in dcf_key))
                    is_dcf = dcf.findall(''.join(valuation_para))
                    #print("\n".join(valuation_para))
                    #print(is_multiple)
                    if is_multiple:
                        eval = "Multiples"
                    if is_dcf:
                        #print("dcf: ", is_dcf)
                        eval = eval+" "+"DCF"
                    dcf_grep = utility.grepDCFinFirstPage(output_filename+"_1page")
                    if dcf_grep == "DCF":
                        if company == "InfineonTechnologiesAG":
                            if "DCF" not in eval:
                                eval = "DCF"
                        else:
                            eval = "DCF"
                        is_dcf = ["DCF"]
                    if is_multiple or is_dcf:
                        return eval.strip()
                else:
                    print("Error:", sys._getframe().f_code.co_name)
                    print("file:", output_filename)
    dcf_grep = utility.grepDCFinFirstPage(output_filename+"_1page")
    if dcf_grep == "DCF" and company == "DeutscheTelekomAG":
        return "DCF"
    ddm_grep = utility.grepDDMinWholeDocs(output_filename)
    if ddm_grep == "DDM" and company == "DPostbank":
        return "DDM"
    return "None"
#-------------------------------------------
def Commerzbank2016(year, company, output_filename):
    #print(sys._getframe().f_code.co_name)
    valuation_header_keyword2016 = ["– reiterate Buy rating","Confirm Buy – raise target price", "– confirm Buy", "confirm Buy,", "Valuation:", "raising our target price"]
    valuation_volkswagen = ["New target price", "Target price and valuation", "Buy rating – target price" , "Buy rating confirmed", "Increase in estimates - Buy rating confirmed", "Buy rating – target price", "Buy rating reiterated", "Valuation:", "Buy rating confirmed – low valuation and improving momentum", "Reiterate Buy recommendation"]
    valuation_bmw = ['our target price', 'our target to', 'target price of', 'Reiterate Add recommendation', 'target price is based', 'valuation:', 'Reiterate Hold rating']
    header = valuation_header_keyword2016 + valuation_volkswagen
    dcf_key = dcf_keyword
    if company == "VolkswagenAG":
        dcf_key = ['DCF derived', 'WACC', 'discounted cash flow', 'DCF']
    if company == "BMW":
        header = valuation_bmw + valuation_header_keyword2016 + valuation_volkswagen
    if company == "Daimler" or company == "MAN" or company == "Hannover" or company == "DeutscheTelekomAG" or company == "MetroAG" or company == "EONSE" or company == "DPostbank" or company == "InfineonTechnologiesAG" or company == "SAPAG":
        header = valuation_bmw + valuation_header_keyword2016 + valuation_volkswagen
        dcf_key = ['DCF derived', 'WACC', 'discounted cash flow', 'DCF']
        if company == "EONSE":
            header = header + ["Target price", "target price", "Sum-of-the-parts", "P/E derived"]
        if company == "MetroAG":
            header = header + ["peer group-derived target price"]
        if company == "InfineonTechnologiesAG":
            header = ["low end", "mid point", "mid-point", "lower end", "a DCF"] + valuation_header_keyword2016 + valuation_volkswagen
    return CommerzbankERBase(header, output_filename, dcf_key, (multiple_keyword + multiple_volkswagen), company)
#-------------------------------------------------
def Commerzbank2013(year, company, output_filename, dcf_key, multiple_key):
    # This return is for Allianz
    return "None"
    #print(sys._getframe().f_code.co_name)
    header = ''
    valuation_header_keyword2013 = []
    header = valuation_header_keyword2013

    # if the first page contain Q\d preview, than its do not contains the summary table. hard to guess
    #print("VKP: ", header)
    return CommerzbankERBase(header, output_filename)

#output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_InfineonTechnologiesAG/2014-03-24IFXGN.DE66132699"
#output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_Hannover/2009-08-06HNRGN.DE50034611"
#print(CommerzbankCorporatesMarkets(2016, "InfineonTechnologiesAG", output_filename))
