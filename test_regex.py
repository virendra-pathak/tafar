import functools as f
import regex
#TP_regex_1 = "[^$](\d*\.\d+)"
TP_regex_1 = "(?<![a-z])(\d*\.\d+)"
TP_regex_2 = "\p{Sc}?(\d*\.\d+|\d+)$"
TP_regex_3 = "\s?TP\s\p{Sc}?(\d*\.\d+|\d+)"
TP_regex_4 = "\p{Sc}?(\d*\.\d+|\d+)/"

def is_line_contains_TP_trigger_word(line, filter_list):
    if any(word in line for word in filter_list):
        print ("True")
    else:
        print ("False")

line = "€123 apple  target Price djdjdjdd hdhdh  Target           x150.00/€10.79           adjustment results from a lower CoE of 9.0% (vs.10% before) and our more positive"
filter_list = ["Target Price", "Target", "target Price"]

print(line)
#is_line_contains_TP_trigger_word(line, filter_list)
result = f.reduce(lambda s, pat: s.split(pat, 1)[0], filter_list, line)
filtered_line = line.replace(result, '')
target_price = regex.findall(TP_regex_1, filtered_line)
print("target Price", target_price)
print("target Price", target_price[0])

