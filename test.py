import os,regex,re
import subprocess

def extract_target_price(output_filename):
	target_price = ''
	with open(output_filename) as f:
		for line in f:
			if "Price Target" in line:
				#print("Line:", line.strip())
				#target_price = re.findall("\d+\.\d+",line.strip())
				target_price = re.findall("\d*\.\d+|\d+",line.strip())
				target_price = ''.join(target_price)
				if not target_price:
					line= next(f)
					#print("Line:", line.strip())
					target_price = regex.findall("\s\p{Sc}?(\d*\.\d+|\d+)[\s|\Z]",line.strip())
					if not target_price:
						line= next(f)
						#print("Line:", line.strip())
						#target_price = regex.findall("\s\p{Sc}?(\d*\.\d+|\d+)[\s|\Z]",line.strip())	
						target_price = regex.findall("\p{Sc}?(\d*\.\d+|\d+)",line.strip())	
					#print(output_filename, "  ", target_price)
				if target_price:
					break
				print("Hey whats wrong with you?")
			if " TP " in line:
				#target_price =''
				#target_price = re.findall("\d+\.\d+",line.strip())
				target_price = regex.findall(r'\s?TP\s\p{Sc}?(\d*\.\d+|\d+)',line.strip())
				target_price = ''.join(target_price)
				print("line: ", line.strip())
				print("TP: ", target_price)
				if target_price:
					break
	f.close()
	return target_price

extract_target_price("/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp2/2016-09-16DBKGN.DE75790841")
