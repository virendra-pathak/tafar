"""Contributor specific module for finding valuation method.\n
Contributor: JP Morgan

.. moduleauthor:: Virendra Kumar Pathak <virendra-kumar.pathak@tum.de>

"""

import sys
import itertools
import regex
import utility

def JPMorganEvaluation(year, company, output_filename):
    """ Returns the valaution method used in report when contributor is JP Morgan.

    Args:
       year (int): year in which report was published
       company (str): the name of the company
       output_filename (str): path of report in text format

    Returns:
       str: valuation method.

    Example:
        >>> print(JPMorganEvaluation(2012, "DeutscheTelekomAG", "<>/2012-08-13DTEGN.F60629285"))
        DCF

    Note:
        Database contains reports from 2005 to 2016.

        The whole set is divided into 3 parts:\n
        1) reports until year 2009\n
        2) reports published between 2009-10 and\n
        3) reports published between 2011-16.\n

        The reason for such division was that some matching templates were found during these time periods.
        Then as per the year, this function call other internal function dedicated to handle report within
        their specific time period (e.g. JPMorganEval2009()).
    """
    eval = ''
    if year < 2009:
        eval = JPMorganEval2009(output_filename, company)
    elif year >= 2009 and year <= 2010:
        eval = JPMorganEval2010(output_filename, company)
    else:
        eval = JPMorganEval2016(company, output_filename)
    return eval

multiple_keyword = ['EV/EBIT', 'EV/EBITA', 'PE ratios', 'multiples', 'PE', 'peer group', '\d*\.\d+x', '\d+x', 'EV/Sales', 'EV/EBITDA','multiple', 'P/BV', 'Multiple', 'P/E']
dcf_keyword = ['DCF derived', 'WACC', 'discounted cash flow', 'DCF']
ddm_keyword = ['dividend discount model', 'DDM']

valuation_header_keyword2009 = ["Valuation:", "target price is based on", "target price based on", "12 month target price", "based on our a sum of parts", "based on our sum of parts", "sum of parts based target price", "based on a sum of parts", "Valuation and recommendation", "Valuation and Rating"]
valuation_header_BMW2009 = ["Valuation:", "target price is based on", "target price based on", "12 month target price", "DCF based", "DCF target", "DCF-based", "DCF/earnings", "Valuation and recommendation"]
valuation_header_Daimler2009 = ["Valuation and recommendation"]
valuation_header_MAN2009 = ["MAN - risks to our rating and target price", "MAN Valuation and risks to target price","Valuation and basis and risks to price target and rating", "MAN: valuation and risks to target price and rating", "valuation and risks", "Valuation and risks"]


def JPMorganEval2009(output_filename, company):
    """ Returns the valuation method for reports by JP Morgan published until 2009.

    Args:
       output_filename (str): path of report in text format
       company (str): the name of the company

    Returns:
       str: valuation method.

    Example:
        >>> print(JPMorganEval2009("DeutscheTelekomAG", "<report-path>"))
        DCF

    Warning:
        Please do not call this function directly.\n
        Instead use parent function JPMorganEvaluation() in the parser.
    """
    #print(sys._getframe().f_code.co_name)
    valuation = ''
    header = valuation_header_keyword2009
    if company == "BMW":
        header = valuation_header_BMW2009
    if company == "Daimler":
        header = valuation_header_BMW2009 + valuation_header_Daimler2009
    if company == "MAN":
        header = valuation_header_MAN2009 +valuation_header_BMW2009 + valuation_header_Daimler2009
    if company == "EONSE":
        header = ["Valuation"] + header
    with open(output_filename) as f:
        for line in f:
            if [i for i in header if i in line]:
                valuation_para = list(itertools.islice(f, 50))
                if valuation_para:
                    multiple = regex.compile('|'.join('(?:{0})'.format(x) for x in multiple_keyword))
                    is_multiple = multiple.findall(''.join(valuation_para))
                    #print("\n".join(valuation_para))
                    dcf = regex.compile('|'.join('(?:{0})'.format(x) for x in dcf_keyword))
                    is_dcf = dcf.findall(''.join(valuation_para))
                    if is_multiple:
                        #print(match)
                        valuation =  "Multiples"
                    if is_dcf:
                        valuation = valuation+" "+"DCF"
                    #TODO: grep blindly the 1st page for DCF
                    dcf_grep = utility.grepDCFinFirstPage(output_filename+"_1page")
                    if dcf_grep == "DCF":
                        valuation = "DCF"
                        is_dcf = ["DCF"]
                    if company == "SAPAG":
                        if "DCF" in valuation:
                            valuation = "DCF"
                    if is_multiple or is_dcf:
                        return valuation.strip()
                else:
                    print("Error:", sys._getframe().f_code.co_name)
                    print("file:", output_filename)
    dcf_grep = utility.grepDCFinFirstPage(output_filename+"_1page")
    if dcf_grep == "DCF":
        return "DCF"
    return "None"

valuation_header_keyword2010 = ["Valuation Methodology and Risks", "Investment Thesis, Valuation and Risks", "based on our sum-of-the-parts", "SOTP based price target", "Valuation and Risks", "DCF-based"]
valuation_header_BMW2010 = ["Valuation Methodology and Risks", "Investment Thesis, Valuation and Risks", "Valuation and Risks"]
def JPMorganEval2010(output_filename, company):
    """ Returns the valuation method for reports by JP Morgan published between 2009-10.

    Args:
       output_filename (str): path of report in text format
       company (str): the name of the company

    Returns:
       str: valuation method.

    Example:
        >>> print(JPMorganEval2010("DeutscheTelekomAG", "<report-path>"))
        Multiples

    Warning:
        Please do not call this function directly.\n
        Instead use parent function JPMorganEvaluation() in the parser.
    """
    #print(sys._getframe().f_code.co_name)
    valuation = ''
    header = valuation_header_keyword2010
    if company == "BMW" or company == "Daimler" or company == "MAN":
        header = valuation_header_BMW2010
    if company == "EONSE":
        header = ["Valuation"] + header
    with open(output_filename) as f:
        for line in f:
            if [i for i in header if i in line]:
                valuation_para = list(itertools.islice(f, 50))
                if valuation_para:
                    multiple = regex.compile('|'.join('(?:{0})'.format(x) for x in multiple_keyword))
                    is_multiple = multiple.findall(''.join(valuation_para))
                    dcf = regex.compile('|'.join('(?:{0})'.format(x) for x in dcf_keyword))
                    is_dcf = dcf.findall(''.join(valuation_para))
                    #print("\n".join(valuation_para))
                    if is_multiple:
                        valuation = "Multiples"
                    if is_dcf:
                        valuation = valuation+" "+"DCF"
                    #TODO: grep blindly the 1st page for DCF
                    dcf_grep = utility.grepDCFinFirstPage(output_filename+"_1page")
                    if dcf_grep == "DCF":
                        valuation = "DCF"
                        is_dcf = ["DCF"]
                    if company == "SAPAG":
                        if "DCF" in valuation:
                            valuation = "DCF"
                    if is_multiple or is_dcf:
                        return valuation.strip()
                else:
                    print("Error:", sys._getframe().f_code.co_name)
                    print("file:", output_filename)
    dcf_grep = utility.grepDCFinFirstPage(output_filename+"_1page")
    if dcf_grep == "DCF":
        return "DCF"
    return "None"

valuation_header_keyword = ["Valuation Methodology and Risks", "Investment Thesis, Valuation and Risks"]
def JPMorganEval2016(company, output_filename):
    """ Returns the valaution method for reports by JP Morgan published between 2011-16.

    Args:
       output_filename (str): path of report in text format
       company (str): the name of the company

    Returns:
       str: valuation method.

    Example:
        >>> print(JPMorganEval2016("DeutscheTelekomAG", "<report_path>"))
        DCF

    Warning:
        Please do not call this function directly.\n
        Instead use parent function JPMorganEvaluation() in the parser.
    """
    valuation = ''
    header = valuation_header_keyword
    if company == "EONSE":
        header = ["Valuation"] + header
    #print(sys._getframe().f_code.co_name)
    with open(output_filename) as f:
        for line in f:
            if "Siemens (SIE GR)" in line:
                #print("Hey I am here: ", output_filename)
                #print(line)
                return (JPMorgan2016_SiemensGR(output_filename))
            #example: DE74882399
            if "Volkswagen Prefs. (VOW3 GR)" in line:
                header = ["Valuation"]

            if [i for i in header if i in line]:
                if "...." in line:
                    continue # go deep in the report. this table of index
                valuation_para = list(itertools.islice(f,50))
                if valuation_para:
                    multiple = regex.compile('|'.join('(?:{0})'.format(x) for x in multiple_keyword))
                    is_multiple = multiple.findall(''.join(valuation_para))
                    dcf = regex.compile('|'.join('(?:{0})'.format(x) for x in dcf_keyword))
                    is_dcf = dcf.findall(''.join(valuation_para))
                    ddm = regex.compile('|'.join('(?:{0})'.format(x) for x in ddm_keyword))
                    is_ddm = ddm.findall(''.join(valuation_para))
                    #print("\n".join(valuation_para))
                    #print(is_multiple)
                    if is_multiple:
                        valuation = "Multiples"
                    if is_dcf:
                        valuation = valuation+" "+"DCF"
                    if is_ddm:
                        valuation = valuation+" "+"DDM"
                    #TODO: grep blindly the 1st page for DCF
                    dcf_grep = utility.grepDCFinFirstPage(output_filename+"_1page")
                    if dcf_grep == "DCF":
                        is_dcf = ["DCF"]
                        if company == "KSAG":
                            valuation = "DCF"
                        else:
                            if "DCF" not in valuation:
                                valuation = valuation + " DCF"
                            #valuation = " DCF"
                    if company == "SAPAG":
                        if "DCF" in valuation:
                            valuation = "DCF"
                    if is_multiple or is_dcf:
                        return valuation.strip()
                else:
                    print("Error:", sys._getframe().f_code.co_name)
                    print("file:", output_filename)
    dcf_grep = utility.grepDCFinFirstPage(output_filename+"_1page")
    if dcf_grep == "DCF":
        return "DCF"
    return "None"

def JPMorgan2016_SiemensGR(output_filename):
    valuation = ''
    #print(sys._getframe().f_code.co_name)
    with open(output_filename) as f:
        for line in f:
            if [i for i in ["Investment Thesis"] if i in line]:
                valuation_para = list(itertools.islice(f,50))
                if valuation_para:
                    multiple = regex.compile('|'.join('(?:{0})'.format(x) for x in multiple_keyword))
                    is_multiple = multiple.findall(''.join(valuation_para))
                    #print("\n".join(valuation_para))
                    dcf = regex.compile('|'.join('(?:{0})'.format(x) for x in dcf_keyword))
                    is_dcf = dcf.findall(''.join(valuation_para))
                    if is_multiple:
                        valuation = "Multiples"
                    if is_dcf:
                        valuation = valuation+" "+"DCF"
                        if company == "BMW":
                            onlyDCF = ["DCF-based", "DCF based"]
                            if [x for x in onlyDCF if x in ''.join(valuation_para)]:
                                eval = "DCF"
                    if is_multiple or is_dcf:
                        return valuation.strip()
                else:
                    print("Error:", sys._getframe().f_code.co_name)
                    print("file:", output_filename)
    return "None"


#output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_DeutscheTelekomAG/2009-03-03DTEGN.F44754892"
#print(JPMorganEvaluation(2009, "DeutscheTelekomAG", output_filename))

#out = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp/2009-08-10ALVG.DE46195582"
##print(JPMorganEvaluation(2010, out))
