"""Contributor specific module for finding valuation method.\n
Contributor: Natixis

.. moduleauthor:: Virendra Kumar Pathak <virendra-kumar.pathak@tum.de>

"""
import sys
import itertools
import regex
import utility

def NatixisEvaluation(year, company, output_filename):
    """ Returns the valaution method used in report when contributor is Natixis.

    Args:
       year (int): year in which report was published
       company (str): the name of the company
       output_filename (str): path of report in text format

    Returns:
       str: valuation method.

    Example:
        >>> print(NatixisEvaluation(2008, "DeutscheTelekomAG", "<path>/2008-02-29DTEGn.DE41764898"))
        Multiples

    Note:
        Database contains reports from 2005-16.
        Then as per the year and/or company name, this function call other internal function
        dedicated to handle report within their specific time period (e.g. Natixis2016()).
    """
    eval = ''
    eval = Natixis2016(company, year, output_filename)
    return eval

#multiple_keyword = ['PE ratios', 'multiples', 'PE', 'P/E', 'peer group', 'peers', '\d*\.\d+x', '\d+x', 'EV/EBIT']
multiple_keyword = ['P/E', 'EV/sales', 'PE', 'Multiple', 'multiplier', '\d*\.\d+x', '\d+x', 'peer valuations', 'EV/EBITDA', 'EV/sales', 'PE-ratio', 'P/E ratio', 'P/E-ratio', 'EV/EBIT', 'EV/Sales', 'valuation multiples', 'multiple', 'EV multiples', 'PE multiples', 'P/BV']
#dcf_keyword = ['DCF derived', 'WACC', 'discounted cash flow', 'DCF']
#dcf_keyword = ['sum-of-the-parts-model','sum of the parts', 'sum-of-the-parts','sum-of-the', 'DDM', 'dividend discount model']
dcf_keyword = ['ROCE', 'WACC', 'DCF']

#-------------------
valuation_header_BMW2016 = ["historical EV/Sales", "EV/EBITDA multiples", "DCF and EV/Sales", "based on a valuation", "a target price of", "EV multiples", "historical EV", "PE multiples", "Valuation:"]
valuation_header_BMW2009 = ["Valuation:"]


#----------------------------------------
def NatixisERBase(company, header, output_filename, dcf_key, multiple_key):
    #print(sys._getframe().f_code.co_name)
    eval = ''
    with open(output_filename) as f:
        #print("Am I inside")
        for line in f:
            if [i for i in header if i in line]:
                valuation_para = list(itertools.islice(f, 20))
                if valuation_para:
                    multiple = regex.compile('|'.join('(?:{0})'.format(x) for x in multiple_key))
                    is_multiple = multiple.findall(''.join(valuation_para))

                    dcf = regex.compile('|'.join('(?:{0})'.format(x) for x in dcf_key))
                    is_dcf = dcf.findall(''.join(valuation_para))
                    #print("\n".join(valuation_para))
                    if is_multiple:
                        eval = "Multiples"
                    if is_dcf:
                        #print("dcf: ", is_dcf)
                        eval = eval+" "+"DCF"
                    dcf_grep = utility.grepDCFinFirstPageNatixis(output_filename+"_1page", company)
                    #print("grep 1 page", dcf_grep)
                    if dcf_grep == "DCF":
                        is_dcf = ["DCF"]
                        if company == "MAN":
                            if "DCF" not in eval:
                                eval = "DCF"
                        else:
                            if company != "DPostbank" or company != "DeutscheTelekomAG":
                                eval = "DCF"
                    if dcf_grep == "DCF + Muliple":
                        return "DCF + Muliple"
                    if is_multiple or is_dcf:
                        return eval.strip()
                else:
                    print("Error:", sys._getframe().f_code.co_name)
                    print("file:", output_filename)
    # Changes done after EONSE (after 30 company)
    # Let assume none of the header matches
    dcf_grep = utility.grepDCFinFirstPageNatixis(output_filename+"_1page", company)
    if dcf_grep == "DCF" and (company != "DPostbank" or company != "DeutscheTelekomAG"):
        return "DCF"
    if dcf_grep and company == "InfineonTechnologiesAG":
        return dcf_grep
    return "None"
#-------------------------------------------
def Natixis2016(company, year, output_filename):
    #print(sys._getframe().f_code.co_name)
    valuation_header_keyword2016 = ["Valuation:", "Investment cases", "Attractive valuation"]
    header = valuation_header_keyword2016
    dcf_key = dcf_keyword
    multiple_key = multiple_keyword
    if company == "BMW" or company == "Daimler" or company == "MAN":
        if year >= 2010:
            header = valuation_header_BMW2016
        else:
            header = valuation_header_BMW2009
    return NatixisERBase(company, header, output_filename, dcf_key, multiple_key)
#-------------------------------------------------

#output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp/2006-03-08ALVG.DE36700091"
#output_filename = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_InfineonTechnologiesAG/2016-08-03IFXGN.DE75372966"
#print(NatixisEvaluation(2016, "InfineonTechnologiesAG", output_filename))
