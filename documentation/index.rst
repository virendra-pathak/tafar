.. Text Analysis of financial analyst research report documentation master file, created by
   sphinx-quickstart on Wed Mar 28 10:41:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directiveAPI .

Welcome to Text Analysis of financial analyst research report's API documentation!
==================================================================================

.. toctree::
   :maxdepth: 2

   intro
   platform
   code
