Installation Platform
=====================

Operating System
----------------
* Ubuntu 16.04 LTS
* gcc (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609
* ldd (Ubuntu GLIBC 2.23-0ubuntu10) 2.23
* Python 3.6.3 :: Anaconda, Inc.

Project specific packages
-------------------------
Please install **miniconda** first. Then rest of the packages using miniconda.
Please have atleast 10 GB space for miniconda and other pakcages.
For miniconda installation, please refer following link: https://conda.io/miniconda.html
Example: conda install <pkg_name>

packages
++++++++
* regex
* langdetect
* xlutils
* xlwt,xlrd
* pandas
