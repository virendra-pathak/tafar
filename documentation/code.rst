Contributors Application Programming Interface (API)
====================================================

Deutsche Bank Equity Research 
-----------------------------

.. automodule:: DeutscheBankEquityResearch
   :members:

Commerzbank Corporates & Markets 
--------------------------------

.. automodule:: CommerzbankCorporatesMarkets
   :members:

JP Morgan
---------

.. automodule:: JPMorganEvaluation
   :members:

Natixis
-------

.. automodule:: NatixisEvaluation
   :members:

UBS 
---

.. automodule:: UBSEvaluation
   :members:

utility (Common Library)
========================

.. automodule:: utility
   :members:

Show Case: Writing and using a parsor
=====================================

Reference for writing a new parser
----------------------------------

Taking Deutsche Telekom parser as a reference
+++++++++++++++++++++++++++++++++++++++++++++

.. automodule:: new_parser_DeutscheTelekomAG
   :members:
