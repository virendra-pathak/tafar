\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}sphinx}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Installation Platform}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Operating System}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Project specific packages}{2}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}packages}{2}{subsection.2.2.1}
\contentsline {chapter}{\numberline {3}Contributors Application Programming Interface (API)}{3}{chapter.3}
\contentsline {section}{\numberline {3.1}Deutsche Bank Equity Research}{3}{section.3.1}
\contentsline {paragraph}{Example}{3}{paragraph*.4}
\contentsline {paragraph}{Example}{4}{paragraph*.6}
\contentsline {paragraph}{Example}{4}{paragraph*.8}
\contentsline {section}{\numberline {3.2}Commerzbank Corporates \& Markets}{4}{section.3.2}
\contentsline {paragraph}{Example}{5}{paragraph*.10}
\contentsline {section}{\numberline {3.3}JP Morgan}{5}{section.3.3}
\contentsline {paragraph}{Example}{5}{paragraph*.12}
\contentsline {paragraph}{Example}{6}{paragraph*.14}
\contentsline {paragraph}{Example}{6}{paragraph*.16}
\contentsline {paragraph}{Example}{7}{paragraph*.18}
\contentsline {section}{\numberline {3.4}Natixis}{7}{section.3.4}
\contentsline {paragraph}{Example}{7}{paragraph*.20}
\contentsline {section}{\numberline {3.5}UBS}{8}{section.3.5}
\contentsline {paragraph}{Example}{8}{paragraph*.22}
\contentsline {chapter}{\numberline {4}utility (Common Library)}{9}{chapter.4}
\contentsline {paragraph}{Example}{9}{paragraph*.24}
\contentsline {paragraph}{Example}{9}{paragraph*.26}
\contentsline {paragraph}{Example}{10}{paragraph*.28}
\contentsline {paragraph}{Example}{10}{paragraph*.30}
\contentsline {paragraph}{Example}{10}{paragraph*.32}
\contentsline {paragraph}{Example}{11}{paragraph*.34}
\contentsline {chapter}{\numberline {5}Show Case: Writing and using a parsor}{12}{chapter.5}
\contentsline {section}{\numberline {5.1}Reference for writing a new parser}{12}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Taking Deutsche Telekom parser as a reference}{12}{subsection.5.1.1}
\contentsline {paragraph}{Example}{12}{paragraph*.36}
\contentsline {paragraph}{Example}{13}{paragraph*.39}
\contentsline {paragraph}{Example}{14}{paragraph*.41}
\contentsline {chapter}{Python Module Index}{15}{section*.42}
\contentsline {chapter}{Index}{16}{section*.43}
