Introduction
============

This document contains API documentations of various contributor module, utility functions and information on using the parser for a particular company.


In addition to existing API documentation, this document also act as a reference point for extending the design to include new contributor and/or new companies in the future


Please refer project report for in-depth information on problem statement, challenges involved in the project and proposed solutions, architecture design and various case studies.

sphinx
------

Most part of this document is auto-generated from docstrings comments in the code using sphinx tool. The **Google** docstring style is used in the document.
For more information on the sphinx please visit following links:

* http://www.sphinx-doc.org/en/stable/ext/napoleon.html
* https://pythonhosted.org/an_example_pypi_project/sphinx.html
* http://www.sphinx-doc.org/en/1.5.1/ext/example_google.html
* http://www.sphinx-doc.org/en/stable/tutorial.html
