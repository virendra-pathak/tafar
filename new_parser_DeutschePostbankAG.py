import os,re
import subprocess
from xlutils.copy import copy
import xlwt,xlrd
import regex
import functools as ftool
import pandas as pd
#from langdetect import detect
from JPMorganEvaluation import JPMorganEvaluation
from UBSEvaluation import UBSEvaluation
from DeutscheBankEquityResearch import DeutscheBankEquityResearch
from CommerzbankCorporatesMarkets import CommerzbankCorporatesMarkets
from NatixisEvaluation import NatixisEvaluation
import utility

database_dir = "/media/virendra/data/study/2sem/idp/code-repo/tafar/dataset/DeutschePostbankAG"
output_dir = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_DeutschePostbankAG/"
file_name = "/media/virendra/data/study/2sem/idp/code-repo/tafar/tmp_DeutschePostbankAG/Deutsche Postbank AG Metadaten.xlsx"

TP_filter_1 = ["Price Target", "Price target", "Target", "price target", "Target Price"]
#TP_filter_2 = ["Price"]
TP_filter_3 = [" TP "]

#TP_regex_1 = "[^$](\d*\.\d+|\d+)"
TP_regex_1 = "[^$](\d*\.\d+)"
TP_regex_2 = "\p{Sc}?(\d*\.\d+|\d+)$"
TP_regex_3 = "\s?TP\s\p{Sc}?(\d*\.\d+|\d+)"
TP_regex_4 = "\p{Sc}?(\d*\.\d+|\d+)/"

def extract_Tprice_from_line_first_line(line, reg_ex, filter_list):
	target_price = ''
	result = ftool.reduce(lambda s, pat: s.split(pat, 1)[0], filter_list, line)
	filtered_line = line.replace(result, '')
	target_price = regex.findall(reg_ex, filtered_line)
	if target_price:
		target_price = ''.join(target_price[0])
	return target_price

def extract_Tprice_from_line(line, reg_ex):
	target_price = ''
	if "$" not in line:
		target_price = regex.findall(reg_ex, line)
		if target_price:
			target_price = ''.join(target_price[-1])
	else:
		target_price = regex.findall(TP_regex_4, line)
		if target_price:
			target_price = ''.join(target_price[-1])
	return target_price

def is_line_contains_TP_trigger_word(line, filter_list):
	if any(word in line for word in filter_list):
		return True
	else:
		return False
	
# Price Target, Price target, TP
def extract_target_price(output_filename, contributor):
	target_price = ''
	filter_1 = TP_filter_1
	if contributor == "JPMorgan":
		filter_1 = ["Price Target"]
	reg_ex_1 = TP_regex_1
	reg_ex_2 = TP_regex_2
	if contributor == "Natixis":
		reg_ex_1 = "\p{Sc}(\d*\.\d+)"
		reg_ex_2 = "\p{Sc}(\d*\.\d+|\d+)$"
	with open(output_filename) as f:
		for line in f:
			#------------------------------
			# "Price Target" or "Price target"
			if is_line_contains_TP_trigger_word(line, filter_1):
				target_price = extract_Tprice_from_line_first_line(line.strip(), reg_ex_1, filter_1)
				if not target_price:
					line = next(f)
					target_price = extract_Tprice_from_line(line.strip(), reg_ex_2)
					if not target_price:
						line = next(f)
						target_price = extract_Tprice_from_line(line.strip(), reg_ex_2)
				if target_price:
					break
				# TODO: move 2 line back as a else part

			# " TP "
			if is_line_contains_TP_trigger_word(line, TP_filter_3):
				#target_price = extract_Tprice_from_line(line.strip(), TP_regex_3)
				target_price = extract_Tprice_from_line(line, TP_regex_3)
				#print(line, "target price", target_price)
				if target_price:
					break
	f.close()
	return target_price

shareDecision1 = ["Sell", "Buy", "Overweight", "Hold", "Neutral", "Underweight", "Add", "Reduce"]
def extract_buysell_decision(output_filename):
	decision = []
	with open(output_filename) as f:
		for line in f:
			decision = [i for i in shareDecision1 if i in line]
			if decision:
				break
	if len(decision) == 1:
		decision = decision[0]
	else:
		decision = ''
	f.close()
	return decision
	
evalDecision = ["DCF based", "DCF-based", "DCF derived", "DCF-derived", "our DCF", "a DCF", "the DCF", "and DCF", "of DCF", "DCF", "WACC", "DCF basis", "DCF analysis", "DCF model", "DCF scenarios", "DCF valuation"]
def extract_evaluation_method(output_filename):
	is_DCF = ''
	with open(output_filename) as f:
		for line in f:
			if regex.findall(r'DCF(?!\smethod)', line):
				is_DCF = "DCF"
				break
	f.close()
	return is_DCF

multiple_list = ["P/E", "P/NAV", "price/EbV", "EV/EBITDAR", "EV/EBIT", "EV/NOPAT", "P/B", "EV/Sales", "EV/Sales","P/S", "PE x", "P/Adj", "Adj. EPS", "EBV", "P/EV", "P/EBV", "sector multiples", "peer multiple", "EV/IC"]
def extract_evaluation_method_multiple(year, contributor, output_filename):
	if contributor == "JPMorgan":
		#print(contributor)
		return JPMorganEvaluation(year, "DPostbank", output_filename)
	if contributor == "UBS Equities":
		return UBSEvaluation(year, "KSAG", output_filename)
	if contributor == "Deutsche Bank Equity Research":
		# Similar profile with Hann, so not changing it here. will save code 
		return DeutscheBankEquityResearch(year, "Hannover", output_filename)
	if contributor == "Commerzbank Corporates & Markets":
		# Since the template is almost same for all years. Well in reality there is no template at all :-(
		return CommerzbankCorporatesMarkets(2016, "DPostbank", output_filename)
	if contributor == "Natixis":
		return NatixisEvaluation(year, "DPostbank", output_filename)
	is_DCF = ''
	#print(contributor)
	count_multiple_hit = 0
	with open(output_filename) as f:
		for line in f:
			# To check if it is DCF
			if not is_DCF:
				if regex.findall(r'DCF(?!\smethod)', line):
					is_DCF = "DCF"
				if [i for i in evalDecision if i in line]:
					id_DCF = "DCF"
			# count number of hits for multiple
			hit_list = [i for i in multiple_list if i in line]
			if hit_list:
				count_multiple_hit = count_multiple_hit + len(hit_list)
	if count_multiple_hit:
		is_DCF = is_DCF + " Multiple"
	return is_DCF


#main function
rb = xlrd.open_workbook(file_name)
r_sheet = rb.sheet_by_index(0)
wb = copy(rb)
w_sheet = wb.get_sheet(0)
#Document ID, row number 5
w_sheet.write(4, 15, 'Target Price')
w_sheet.write(4, 16, 'Decision')
#w_sheet.write(4, 17, 'Evaluation Method')
w_sheet.write(4, 17, 'Evaluation')
w_sheet.write(4, 18, 'Language')


for file in os.listdir(database_dir):
	if file.endswith(".pdf"):
		#extracting document id e.g.DE48994574
		document_id = re.findall(r'\.(.*?)\.',file)
		if document_id:
			document_id = document_id[0]
		#extracting the pdf first page
		full_file_path = os.path.join(database_dir, file)
		#--------------------------
		file_basename = os.path.splitext(file)[0]
		output_filename = output_dir+file_basename
		output_filename_1page = output_dir+file_basename+"_1page"
		#--------------------
		cmd_1page = ["pdftotext", "-l", "1", "-layout", full_file_path,output_filename_1page]
		cmd = ["pdftotext", "-layout", full_file_path,output_filename]
		#print(full_file_path)
		#print(output_filename)
		text_1page = subprocess.check_output(cmd_1page, universal_newlines=True,stderr=subprocess.STDOUT)
		text = subprocess.check_output(cmd, universal_newlines=True,stderr=subprocess.STDOUT)
		#print(text)
		#extracting the target price
		year = utility.getYearOfReport(r_sheet, document_id)
		contributor = utility.getContributorOfReport(r_sheet, document_id)
		target_price = extract_target_price(output_filename_1page, contributor)
		shareDecision = extract_buysell_decision(output_filename)
		#evalDecision = extract_evaluation_method(output_filename)
		evalmultiple_count = extract_evaluation_method_multiple(year, contributor, output_filename)
		#language = utility.detect_language(output_filename)
		language = "en"
		#print(shareDecision)
		utility.updateExcelSheet(r_sheet, w_sheet, document_id, target_price, shareDecision, evalDecision, evalmultiple_count, language)
		#os.remove(output_filename_1page)

#book.save("report.xls")
final_output_file = os.path.splitext(file_name)[0] + '-out' + os.path.splitext(file_name)[-1]
wb.save(final_output_file)
#ExcelFileReader()
msft = pd.read_excel(final_output_file, header=4)
#msft = msft[msft['Second. Ticker'] == 'n/a']
msft = msft[msft['Second. Ticker'].str.contains("^[^,]+$")]
#print(msft['Target Price'])
msft = msft[msft['Prim. Ticker'].str.contains("^[^,]+$")]
msft = msft[msft['Prim. Ticker'].str.contains("^[^,]+$")]
msft = msft[msft['Language'] == 'en']
#for x in msft['Target Price']:
#	print(type(x))
msft.to_excel(final_output_file, startrow=4, startcol=0, index=False)

